ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

install:
	install -d $(DESTDIR)$(PREFIX)/sbin/
	install -m 755 build $(DESTDIR)$(PREFIX)/sbin/ 
	install -m 755 specialize $(DESTDIR)$(PREFIX)/sbin/ 

uninstall:
	rm $(DESTDIR)$(PREFIX)/sbin/build
	rm $(DESTDIR)$(PREFIX)/sbin/specialize

