# Install Wordpress and deps
chroot ./rfs /bin/bash -c "
	apt install -y wordpress curl wget
"

# Write wordpress Apache sites-available config file
# Includes both HTTP and HTTPS
cat <<-END > ./rfs/etc/apache2/sites-available/wp.conf
<VirtualHost *:80>
	ServerName localhost

	DocumentRoot /usr/share/wordpress

	Alias /wp-content /var/lib/wordpress/wp-content
	<Directory /usr/share/wordpress>
		Options FollowSymLinks
		AllowOverride Limit Options FileInfo
		DirectoryIndex index.php
		Require all granted
	</Directory>
	<Directory /var/lib/wordpress/wp-content>
		Options FollowSymLinks
		Require all granted
	</Directory>

	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost> 

<VirtualHost *:443>
	DocumentRoot /usr/share/wordpress
	Alias /wp-content /var/lib/wordpress/wp-content
		<Directory /usr/share/wordpress>
			Options FollowSymLinks
			AllowOverride Limit Options FileInfo
			DirectoryIndex index.php
			Require all granted
		</Directory>
		<Directory /var/lib/wordpress/wp-content>
			Options FollowSymLinks
			Require all granted
		</Directory>
	ServerAdmin webmaster@localhost
	SSLEngine on
	SSLCertificateFile /etc/ssl/localhost.crt
	SSLCertificateKeyFile /etc/ssl/localhost.key
</VirtualHost> 
END

# Install SSL/TLS-cert
mkdir ./rfs/etc/ssl/
cp ./components/certs/* ./rfs/etc/ssl/

# Enable wordpress site, configure Apache
# 
chroot ./rfs /bin/bash -c "
	ln -s /etc/apache2/sites-available/wp.conf /etc/apache2/sites-enabled/
	a2enmod ssl

	cd /usr/share
	wget https://wordpress.org/latest.tar.gz
	tar xf latest.tar.gz
"

# write init.sh
cat <<-END > ./rfs/init.sh
#!/bin/bash
echo "<?php
	define('DB_NAME', '\${DB_NAME}');
	define('DB_USER', '\${DB_USER}');
	define('DB_PASSWORD', '\${DB_PASSWORD}');
	define('DB_HOST', '\${DB_HOST}');
	define('WP_CONTENT_DIR', '/var/lib/wordpress/wp-content');
	define('WP_DEBUG', true);
?>" > /etc/wordpress/config-default.php

apachectl -D FOREGROUND
END

# Cleanup
chroot ./rfs /bin/bash -c "
	rm /etc/apache2/sites-available/000-default.conf
	rm /etc/apache2/sites-enabled/000-default.conf
	rm -rf /var/www
"

if [ $createDockerFile ];
then
	cat <<-END > ./Dockerfile
	FROM scratch
	COPY ./rfs /

	EXPOSE 80 443

	RUN chown -R www-data:www-data /usr/share/wordpress
	RUN chown -R www-data:www-data /var/lib/wordpress
	RUN chown -R www-data:www-data /etc/wordpress
	CMD ["/bin/bash", "/init.sh"]
	END
fi

