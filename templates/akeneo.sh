phpver="7.3"

./fakerfs --mount

# Install dependencies
chroot $rfs /bin/bash -c "
	apt-get update

	apt-get -y install php7.3-cli php7.3-apcu php7.3-bcmath php7.3-curl php7.3-fpm php7.3-gd php7.3-intl php7.3-mysql php7.3-xml php7.3-zip php7.3-zip php7.3-mbstring php7.3-imagick php7.3-exif apache2 nodejs composer git sed unzip wget curl yarn yarnpkg make
"

# Configure
chroot $rfs /bin/bash -c "
	a2enmod rewrite proxy_fcgi
"

# Try the composer thing
chroot $rfs /bin/bash -c "
	mv /usr/bin/composer /usr/local/bin/composer

	mkdir -p /var/www/akeneo
	php -d memory_limit=8G /usr/local/bin/composer create-project --prefer-dist \
		akeneo/pim-community-standard /var/www/akeneo \"4.0.*@stable\"
"

# Configure configs
chroot $rfs /bin/bash -c "
	### php-cli
	sed -i 's/memory_limit = -1/memory_limit = 1024M/g' /etc/php/$phpver/cli/php.ini
	sed -i 's/;date.timezone =/date.timezone = Europe\/Amsterdam/g' /etc/php/$phpver/cli/php.ini
	### php-fpm
	sed -i 's/memory_limit = 128M/memory_limit = 512M/g' /etc/php/$phpver/fpm/php.ini
	sed -i 's/;date.timezone =/date.timezone = Europe\/Amsterdam/g' /etc/php/$phpver/fpm/php.ini
"

chroot $rfs /bin/bash -c "
rm /etc/apache2/sites-available/* 
rm /etc/apache2/sites-enabled/*

cat << END > /etc/apache2/sites-available/akeneo-pim.local.conf

<VirtualHost *:80>
    ServerName akeneo-pim.local

    DocumentRoot /var/www/akeneo/public
    <Directory /var/www/akeneo/public>
        AllowOverride None
        Require all granted

        Options -MultiViews
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^(.*)\$ index.php [QSA,L]
    </Directory>

    <Directory /var/www/akeneo/public/bundles>
        RewriteEngine Off
    </Directory>

    <FilesMatch \\.php$>
        SetHandler \"proxy:unix:/run/php/php7.3-fpm.sock|fcgi://localhost/\"
    </FilesMatch>

    SetEnvIf Authorization .+ HTTP_AUTHORIZATION=\$0

    ErrorLog /akeneo-pim_error.log
    LogLevel warn
    CustomLog /akeneo-pim_access.log combined
</VirtualHost>
END

a2ensite akeneo-pim.local
"

# Init script
chroot $rfs /bin/bash -c "
	cat <<-END > /init.sh
		#!/bin/bash

		cat <<-EOF > /var/www/akeneo/.env
			APP_ENV=\\\${APP_ENV}
			APP_DEBUG=\\\${APP_DEBUG}
			APP_DATABASE_HOST=\\\${APP_DATABASE_HOST}
			APP_DATABASE_PORT=\\\${APP_DATABASE_PORT}
			APP_DATABASE_NAME=\\\${APP_DATABASE_NAME}
			APP_DATABASE_USER=\\\${APP_DATABASE_USER}
			APP_DATABASE_PASSWORD=\\\${APP_DATABASE_PASSWORD}
			APP_DEFAULT_LOCALE=\\\${APP_DEFAULT_LOCALE}
			APP_SECRET=\\\${APP_SECRET}
			APP_INDEX_HOSTS=\\\${APP_INDEX_HOSTS}
			APP_PRODUCT_AND_PRODUCT_MODEL_INDEX_NAME=\\\${APP_PRODUCT_AND_PRODUCT_MODEL_INDEX_NAME}
			MAILER_URL=\\\${MAILER_URL}
			AKENEO_PIM_URL=\\\${AKENEO_PIM_URL}
			APP_ELASTICSEARCH_TOTAL_FIELDS_LIMIT=\\\${APP_ELASTICSEARCH_TOTAL_FIELDS_LIMIT}
		EOF
	
		export NODE_PATH=/usr/lib/nodejs:/usr/share/nodejs
		
		cd /var/www/akeneo
		NO_DOCKER=true make dev
	
		printf \"Configuring file permissions, this will take a while..\"
		chown -R www-data:www-data /var/www
		chmod -R 755 /var/www

		php-fpm7.3 &
		apachectl -D FOREGROUND
	END
"

# cleanup
chroot $rfs /bin/bash -c "
	rm -rf /root/.composer
	apt-get clean
"

./fakerfs --unmount
