### This is a reference custom bash script
### This script can be loaded in to specialize with the --customize parameter
### This script can be used to further customize a specialization
### #!/bin/bash is not required since this script will be loaded in to specialize which already calls it


# Run commands inside of container
chroot ./root /bin/bash -c "
	apt update
	apt install apache
"

# Run commands on host system
mkdir ./root/run/php
