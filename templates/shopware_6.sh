# Install dependencies, tools and shopware
chroot ./rfs /bin/bash -c "
	apt install -y wget \
		unzip \
		php-zip \
		php-pdo-mysql \
		php-curl \
		php-mbstring \
		php-xml \
		php-intl \
		php-gd 

	mkdir /var/www/shopware
	cd /var/www/shopware
	wget https://www.shopware.com/en/Download/redirect/version/sw6/file/install_v6.3.2.0_979aa4e7fd33d75e33ce1dc58b65616b18b702d2.zip -O /var/www/shopware/shopware_6.zip

	unzip /var/www/shopware/shopware_6.zip
"

# Configure Apache webserver
echo "
<VirtualHost *:80>  
 DocumentRoot /var/www/shopware/public 

 <Directory /var/www/shopware> 
  Options Indexes FollowSymLinks MultiViews 
  AllowOverride All 
  Order allow,deny 
  allow from all 
 </Directory> 

 LogLevel debug 
</VirtualHost>
" > ./rfs/etc/apache2/sites-available/shopware.conf

# Configure Apache
chroot ./rfs /bin/bash -c "
	a2enmod rewrite

	ln -s /etc/apache2/sites-available/shopware.conf /etc/apache2/sites-enabled/
"

# Cleanup
chroot ./rfs /bin/bash -c "
	apt purge -y wget \
		unzip

	rm /var/www/shopware/shopware_6.zip
	rm -rf /var/www/html
	rm /etc/apache2/sites-available/000-default.conf
	rm /etc/apache2/sites-available/default-ssl.conf
	rm /etc/apache2/sites-enabled/000-default.conf
"

if [ $createDockerFile ];
then
	cat <<-END > ./Dockerfile
	FROM scratch
	COPY ./rfs /

	EXPOSE 80

	RUN chown -R www-data:www-data /var/www
	CMD ["/bin/bash", "/init.sh"]
	END
fi

