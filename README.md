### Dependencies
- `debootstrap`
- `dpkg`
- `bash`        

# Parameters
### Build
| Option | Info |
| ------ | ---- |
| `--tar` | Create a tarball of download |
| `--unpack` | Unpack tarball instead of downloading packages |

### Specialize
| Option | Info |
| ------ | ---- |
| `--dockerfile` | Dispense default Dockerfile for chosen specialization |
| `--customize` | Load *.sh from current directory |

# Usage
### Creating the Debian root file system
Run `build stable` to build the current stable version of Debian.

You can also request other versions of Debian by their release name such as 
Buster, Squeezy, Sid, testing etc..

When you are often rebuilding the root file system it might be nice to not have to download the Debian installation
files every time you build a new root. You can store the downloaded files in
a tar.gz file by calling the script with the `--tar` parameter like so `build
stable --tar`.

If you already have an existing tarball downloaded it can be extracted 
with `build stable --unpack`.

### Specializing the Debian Linux root file system
Use the `specialize` script and select a basic specialization. See the
`specializations` directory for all available specialization scripts. All these
scripts can be called by name as the first parameter of specialize. For 
example `specialize apache`

Customization scripts can be used to extend the functionality of specializations. 
Load in a customization script with `--customize`, `specialize` will look for
any file matching the pattern defined in it's variable config (default *.sh)
in the current directory. So copy the customization from the `templates` 
folder next to `rfs` and `specialize`, or alternatively create a symbolic link `ln -s templates/apache.sh`.

A default dockerfile for the selected specialization can be requested with 
`--dockerfile`. A simple Dockerfile which could either be run as-is or be edited by the user
will be outputed. The Dockerfile will be dispensed by the customization script, currently not all specialization scripts have a Dockerfile.

### Manually entering the Debian root file system
Use `chroot rfs` to change in to the Debian root filesystem, here you can 
execuate commands like you are on a normal Debian installation.

Some programs require /dev, /proc or /sys to be available. Use `fakerfs --mount` to quickly mount those folders from your host machine on to the root file system. Be sure to use `fakerfs --unmount` before removing the file system or putting it in a container. 

When Docker starts the container it will call `/init.sh` to launch all
dependencies and the program it will track, create/edit this script to change 
the launch behaviour of the container.

Programs can be launched and detached from the shell by following them with a 
`&`, for  example `nginx &`. Use this to launch multiple programs on the root 
file system for testing.

Some programs launch in multiple steps, with parameters or via a daemon. You 
can use the Systemd init scripts location in `/usr/lib/systemd/system` as 
reference on how to launch programs. 

To quit running programs make use of a process manager such as `htop` running 
with root privileges on the host system.

# Extra functionality
### Installing the scripts
A Makefile is included for easy installation and uninstallation of the scripts.
Use `make install` and `make uninstall`.

# Development and usage tips
### Verbose log output in real time
If you want to have a verbose log output in real time you can watch the .log 
file, `watch -n 0.1 tail -n $(tput lines) .log`. 

### Use scripts with other distros
These scripts are written to be universal and should work with any Debian-based Linux distribution. See `components/config` for the script configuration files.

# Troubleshooting
### build script stuck on package or error can't download package
<kbd>CTRL</kbd> + <kbd>C</kbd> to quit and try again, the Debian repos can be 
finicky sometimes and the debootstrap script lacks error handeling in case
this happens.
